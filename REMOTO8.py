#Questão1
def freq_palavras(frases):
    '''Função que retorna um dicionario com a palavra e quantas vezes ela se repete na frase'''
    liston = frases.split()
    dicioboy = {}
    for palavra in liston:
        chabe = liston.count(palavra)
        dicioboy[palavra] = chabe
    return dicioboy

#Questão2
def total(lista,dicionario):
    '''Função que retorna a conta do supermercado da compra de cada item da lista de compras'''
    sigma = 0 
    for coisa in lista:
        if coisa in dicionario:
            sigma += dicionario[coisa]
    return round(sigma,2)

#Questão3
def lingua_p(palabra):
    
    
    '''função que retorna palavra traduzida na língua do p.
    str--> str'''
    listola = []
    palabraNoba = [] 
    for letra in palabra:
        '''geração de lista com cada letra da palavra'''
        listola.append(letra)
    for vogogal in listola:  
        if vogogal in 'aeiouáéíóú':  
            palabraNoba.append(vogogal + 'p' + vogogal)  
        else: 
            palabraNoba.append(vogogal)
    
    linguaP = ''.join(palabraNoba)
    return linguaP.lower()

#Questão4
def qtd_divisores(numero):
    '''Função que retorna quantos divisores um numero possui'''
    dibisores = []
    QtDibisores = 0
    dibisoresPossibeis= list(range(1,numero+1))
    
    for dibisor in dibisoresPossibeis:
        if numero%dibisor == 0:
            dibisores.append(dibisor)
    QtDibisores = len(dibisores)
    return QtDibisores

#Questão5
def primo(numero):
    '''Função que retorna se um numero é primo ou não'''
    dibisores = []
    QtDibisores = 0
    dibisoresPossibeis= list(range(1,numero+1))
    ehPrimo = False
    '''reaproveitei boa parte da função anterior aqui'''
    for dibisor in dibisoresPossibeis:
        if numero%dibisor == 0:
            dibisores.append(dibisor)
            
    if len(dibisores) <= 2:
        ehPrimo = True
        return ehPrimo
    else:
        ehPrimo = False
        return ehPrimo
    
#Questão6
def soma_h(N):
    
    '''função retorna somatório de frações com
    N termos onde cada denominador é uma PA crescente de r = 1.
    int--> float'''
    H = [1]  
    for numero in range(2, N+1):
        H.append((numero)**-1)  
        sigma = sum(H)
    return round(sigma, 2)
